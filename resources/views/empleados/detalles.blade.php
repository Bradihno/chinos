<!--Extender la masterpage en esta vista-->
@extends('layouts.masterpage')

@section('contenido')
  <h2>Detalles de empleado</h2>
 
  <div class="card" style="width:400px">
    
    <div class="card-header">
      <h4 class="card-title">{{ $empleado->FirstName}} {{ $empleado->LastName}}</h4>
    </div> 
      
      <div class="card-body">
      
          <!--Info de empleado en list item-->
          <ul class="list-group">
            <li class="list-group-item text-danger">{{ $empleado->Title }}  </li>
            
            @if($empleado->jefe_directo()->get()->count()!==0)
            <li class="list-group-item text-success">
              Jefe directo: 
               {{ $empleado->jefe_directo()->first()->FirstName }}
               {{ $empleado->jefe_directo()->first()->LastName }}  
            </li>
            @endif

            <li class="list-group-item text-success">
               Fecha nacimiento: {{$empleado->BirthDate->format('l jS \\of F Y h:i:s A')}}
            </li>
            <li class="list-group-item text-bold">
               Fecha contratacion: {{$empleado->HireDate->format('l jS \\of F Y h:i:s A')}}
            </li>
<br/>

          </ul>
       </div>
  </div>
  <br>

@if ($empleado->clientes()->get()->count()!==0)
 <h3>Clientes atendidos: </h3>
   <table class="table table-hover">
     <thead>
       <tr>
         <th>Nombre</th}>
         <th>Compañía</th>
         <th>Email</th>
       </tr>
      </thead>
     <tbody>
       @foreach ($empleado->clientes()->get() as $cliente)
          <tr>
             <td>{{ $cliente->FirstName}} {{$cliente->LastName}}</td>
            @if ($cliente->Company)
             <td>{{$cliente->Company}}</td>
            @else
            <td><strong class="text-danger">INDEPENDIENTE</td></strong>
            @endif
             <td>{{$cliente->Email}}</td>
          </tr>
       @endforeach
     </tbody>
   </table>
 @else

 @endif
@endsection
