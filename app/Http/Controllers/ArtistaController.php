<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Artista;
use Illuminate\Support\Facades\Validator;


class ArtistaController extends Controller
{
    //Accion: Metodo en un controlador 
    // asociado a una ruta
    //La programación en un caso de uso

    public function index(){
        //Obtener datos, utilizar modelo
        $listado_artistas = Artista::all();


         //Presentar vista con los artistas de la bd
         return view('artistas.index')->with("artistas", $listado_artistas);
    }

    //Mostrar el formulario de creacion de artista
    public function create() {
        return view('artistas.new');
    }

    //Captura los datos del cliente(Formulario)
    public function store(Request $request) {
      /* echo "<pre>";
        var_dump($_POST);
        echo "</pre>";*/

        //Validacion paso 1 - establecer reglas de validacion
                            //para cada campo

        $reglas = [
            "nombre_artista" => ['required', 'alpha', 'min:3', 'max:15'. 'unique:artists,Name']
        ];

        //Validacion paso 2 -Crear objeto validador: fdatos a validar y atos de validador
        $validador = Validator::make($request->all(), $reglas);


        //Velidacion paso 3: -VaLIDAR Y ESTABLECER ACCIONES
        if($validador->fails(8)){
            //Acciones cuando la validacion falla
           //Redirigir a la vista con el validador
           return redirect('artistas/create')->withErrors($validador);



        }/*else{
            //Acciones cuando los datos son correctos

        }*/


        //Guardar el artista utilizado en el modelo
        $a = new Artista();
        $a->Name = $request->nombre_artista;
        $a->save();

        //Mensaje a la vista
        //Redireccionamiento: a la ruta que muestra la vista
        //With perimite crear una flass session: variable de sesion que va a durar un solo request
        //Con el nombre "exito"
        return redirect('artistas/create')
        ->with("exito" , "Artista fue registrado correctamente")
        ->with("Nombre artista" , $a->Name);
        

    }

}